import React, { Fragment, Component } from 'react';
import HeroPage from '../layout/HeroPage';
import ServicePage from '../layout/ServicePage';
import SecurityPage from '../layout/SecurityPage';
import SavingPage from '../layout/SavingPage';
import CustomerPage from '../layout/CustomerPage';
import HostingPage from '../layout/HostingPage';
import ClientPage from '../layout/ClientPage';
import FAQPage from '../layout/FAQPage';
import GuaranteePage from '../layout/GuaranteePage';
import PaymentPage from '../layout/PaymentPage'

class Card extends React.Component {
	state = {
		heropage: [
		{
			id: 1,
			title: 'Unlimited Web Hosting Terbaik di Indonesia',
			description: "Ada banyak peluang bisa Anda raih dari rumah dengan memiliki website. Manfaatkan diskon hosting hingga 75% dan tetap produktif di bulan Ramadhan bersama niagahoster.",
			descriptionChild: 'Yuk segera order karena diskon dapat berakhir sewaku-waktu!',
			countTime: '00 : 00 : 00 : 00',

			button: 'Pilih Sekarang',
			image: require('../asset/image/hero-home-ramadhan.webp'),
		
		},
		],

		servicepage: [
		{
			id: 2,
			title: 'Layanan Niagahoster',

			imageicon1: require('../asset/image/icon-1.svg'),
			titleCard: 'Unlimited Hosting',
			description: 'Cocok untuk website skala kecil dan menengah',
			descriptionChild: 'Mulai dari',
			price: 'Rp 10.000,-',

			
			imageicon2: require('../asset/image/icons-cloud-hosting.svg'),
			titleCard2: 'Cloud Hosting',
			description2: 'Kapasitas resource tinggi, fully managed, dan mudan dikelola',
			descriptionChild2: 'Mulai dari',
			price2: 'Rp 150.000,-',
		
		
			imageicon3: require('../asset/image/icons-cloud-vps.svg'),
			titleCard3: 'Domain',
			description3: 'Temukan nama domain yang anda inginkan',
			descriptionChild3: 'Mulai dari',
			price3: 'Rp 14.000,-',
	
	
			imageicon4: require('../asset/image/icons-domain.svg'),
			titleCard4: 'Unlimited Hosting',
			description4: 'Cocok untuk website skala kecil dan menengah',
			descriptionChild4: 'Mulai dari',
			price4: 'Rp 10.000,-',
			
		
			imageicon5: require('../asset/image/icons-cloud-hosting.svg'),
			titleCard5: 'Pembuatan Website',
			description5: '500 perusahaan lebih percayakan pembuatan websitenya pada kami',
			descriptionChild5: 'Cek Selengkapnya',

		},
		],

		securitypage: [
		{
			id: 3,
			title: 'Prioritas Kecepatan dan Keamanan',

			titleCard: 'Hosting Super Cepat',
			description: ' Pengunjung tidak suka website lambat. Dengan dukungan LiteSpeed Web Server, waktu loading website Anda akan meningkat pesat.',

			titleCard1: 'Keamanan Website Extra',
			description1: ' Teknologi keamanan Imunify 360 memungkinkan Website anda terlindung dari serangan hacker, malware, dan virus berbahaya setiap saat.',

			seeMore: 'Lihat Selengkapnya',

			imageaa1: require('../asset/image/hosting-super-cepat.svg'),
			imageaa2: require('../asset/image/domain-keamanan-ekstra.svg'),

			imagea1: require('../asset/image/server.webp'),
			imagea2: require('../asset/image/graphic.svg'),
			imagea3: require('../asset/image/imunify.svg'),
			imagea4: require('../asset/image/lite-speed.svg'),


		},
		],

		savingpage: [
		{
			 id: 4,
			 title: 'Biaya Hemat Kualitas Hebat',

			 imagebb: require('../asset/image/icons-domain-harga-murah.svg'),
			 imagebb1: require('../asset/image/icons-website-selalu-online.svg'),
			 imagebb2: require('../asset/image/icons-domain-support-andal.svg'),

			 imageb:  require('../asset/image/titis.webp'),
			 imageb1: require('../asset/image/woman1.webp'),
			 imageb2: require('../asset/image/woman2.png'),
			 imageb3: require('../asset/image/man.png'),
			 imageb4: require('../asset/image/icon-online.svg'),
			 imageb5: require('../asset/image/intercom-logo.svg'),

			 ptitle: 'Harga Murah Fitur Lengkap',
			 pdesc: 'Anda bisa berhemat dan tetap mendapatkan hosting terbaik dengan fitur lengkap, dari auto install Wordpress, cPanel lengkap, hingga SSL gratis',

			 ptitle1: 'Website Selalu Online',
			 pdesc1: 'Jaminan server uptime 99,98% memungkinkan website Anda selalu online sehingga Anda tidak perlu khawatir kehilangan traffic dan pendapatan',

			 ptitle2: 'Tim Support Handal dan Cepat Tanggap',
			 pdesc2: 'tidak perlu menunggu lama, selesaikan masalah Anda dengan cepat secara real-time melalui live chat 24/7',
		},
		],

		customerpage: [
		{
			 id: 5,
			 title: 'Kata Pelanggan Tentang Niagahoster',

			 image: require('../asset/image/devjavu.webp'),
			 description: 'Website itu sangat penting bagi UMKM sebagai sarana promosi untuk memenangkan persaingan di era digital',
			 nameowner: 'Didik & Johan',
			 owner: ' Owner Devjavu',

			 image1: require('../asset/image/optimizer.webp'),
			 description1: 'Bagi saya Niagahoster bukan sekedar penyedia hosting, melainkan partner bisnis yang bisa dipercaya',
			 nameowner1: 'Bob Setyo',
			 owner1: ' Owner Digital Optimizer Indonesia',

			 image2: require('../asset/image/sateratu.webp'),
			 description2: 'Solusi yang diberikan tim support Niagahoster sangat mudah dimengerti buat saya yang tidak paham teknis',
			 nameowner2: 'Budi Saputro',
			 owner2: ' Owner Sate Ratu',
			
		},
		],

		hostingpack: [
		{
			id: 6,
			title: 'Pilih Paket Hosting Anda',

			discounttitle:'Termurah!',
			namediscount:'Bayi',
			price:'Rp 10.000',
			button:'PILIH SEKARANG',
			description:'Sesuai untuk Pemula atau Belajar Website',
			detail:'Lihat detail fitur',

			discounttitle1:'Diskon up to 34%',
			namediscount1:'Pelajar',
			price1:'Rp 60.800',
			button1:'PILIH SEKARANG',
			description1:'Sesuai untuk BudgetMinimal, Landing Page, Blog Pribadi',
			detail:'Lihat detail fitur',

			discounttitle2:'Diskon up to 75%',
			namediscount2:'Personal',
			price2:'Rp 26.563',
			button2:'PILIH SEKARANG',
			description2:'Sesuai untuk Website Bisnis, UKM, Organisasi, Komunitas, Toko Online, dll',	
			detail:'Lihat detail fitur',

			discounttitle3:'Diskon up to 42%',
			namediscount3:'Bisnis',
			price3:'Rp 85.724',
			button3:'PILIH SEKARANG',
			description3:'Sesuai untuk Website Bisnis, Portal Berita, Toko Online, dll',
			detail:'Lihat detail fitur',
		},
		],

		clientpage: [
		{
			id: 7,
			title: 'Dipercaya 52.000+ Pelanggan di Seluruh Indonesia',

			image1: require('../asset/image/richeese.svg'),
			image2: require('../asset/image/rabbani.svg'),
			image3: require('../asset/image/oten.svg'),
			image4: require('../asset/image/hydro.svg'),
			image5: require('../asset/image/petro.svg'),

		},
		],

		faqpage: [
		{
			id: 8,
			title: 'Pertanyaan yang Sering Diajukan',
			image: require('../asset/image/icon-plus.svg'),

			label1: 'Apa itu web hosting?',
			description1:'',
			label2: 'Mengapa saya harus menggunakan web hosting Indonesia?',
			description2:'',
			label3: 'Apa yang dimaksud dengan Unlimited Hosting di Niagahoster?',
			description3:'',
			label4: 'Paket web hosting mana yang tepat untuk saya?',
			description4:'',
			label5: 'Apakah semua pembelian hosting mendapatkan domain gratis?',
			description5:'',
			label6: 'Jika sudah memiliki website, apakah saya bisa transfer web hosting ke Niagahoster?',
			description6:'',
			label7: 'Apa saja layanan web hosting Niagahoster?',
			description7:'',

		},
		],

		guaranteepage: [
		{
			id: 9,
			image: require('../asset/image/icons-guarantee.svg'),
			title: 'Garansi 30 Hari Uang Kembali',
			description:'Tidak puas dengan layanan hosting Niagahoster? Kami menyediakan garansi uang kembali yang berlaku 30 hari sejak tanggal pembelian',
			button: 'MULAI SEKARANG',

		},
		],

		paymentpage: [
		{
			id: 10,
			title: 'Beragam pilihan cara pembayaran yang mempercepat proses order hosting & domain Anda',

			image1: require('../asset/image/bca.svg'),
			image2: require('../asset/image/mandiri.svg'),
			image3: require('../asset/image/bni.svg'),
			image4: require('../asset/image/bri.svg'),
			image5: require('../asset/image/bii.svg'),
			image6: require('../asset/image/cimb.svg'),
			image7: require('../asset/image/alto.svg'),
			image8: require('../asset/image/atm-bersama.svg'),
			image9: require('../asset/image/paypal.svg'),
			image10: require('../asset/image/indomart.svg'),
			image11: require('../asset/image/alfamart.svg'),
			image12: require('../asset/image/pegadaian.svg'),
			image13: require('../asset/image/pos.svg'),
			image14: require('../asset/image/ovo.svg'),
			image15: require('../asset/image/gopay.svg'),
			image16: require('../asset/image/visa.svg'),
			image17: require('../asset/image/master.svg'),

		},
		],


		
	}

render () {
	return (
		<Fragment>
			<div>
				<HeroPage heropage={this.state.heropage}/>
				<ServicePage servicepage={this.state.servicepage}/>
				<SecurityPage securitypage={this.state.securitypage}/>
				<SavingPage savingpage={this.state.savingpage}/>
				<CustomerPage customerpage={this.state.customerpage}/>
				<HostingPage hostingpack={this.state.hostingpack}/>
				<ClientPage clientpage={this.state.clientpage}/>
				<FAQPage faqpage={this.state.faqpage}/>
				<GuaranteePage guaranteepage={this.state.guaranteepage}/>
				<PaymentPage paymentpage={this.state.paymentpage}/>
			</div>
		</Fragment>
		);
	}
};



export default Card;
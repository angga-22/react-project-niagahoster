import React from 'react';
import { Link } from 'react-router-dom';
import '../asset/style/HeroPage.css'

function HeroPage(props) {
	const heropage = props.heropage.map((item) => (
		<div className='heropage' key={item.id}>
		 <header className="document-header-wrapper">
	      <section className="contact-header">
	        <section className="hero container flex">
	          <section className="hero-left">
				<h1 className='hero-title'>{item.title}</h1>
				<p className='hero-description'>{item.description}</p>
				<p>{item.descriptionChild}</p>
				<p className='countdown '>{item.countTime}</p>
				<Link to='/' className='btn-hero orange'>{item.button}</Link>
			  </section>
			  <section className="hero-right">
			  	<div className='hero-img-wrapper'>
					<img src={item.image} alt='' className='hero-img'/>
				</div>
			  </section>
	        </section>
	      </section>
	     </header>
		</div>
		));

		return (
			<div>
				{heropage}
			</div>	
			)
		}

export default HeroPage;
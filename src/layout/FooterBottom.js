import React from "react";
import { Link } from 'react-router-dom';
import '../asset/style/FooterBottom.css';
import '../asset/style/All.css';

const FooterTop = () => {
    return (
    <footer>
      <div className="container">
        <div className="footer-content-wrapper flex">
          <div className="footer-content-list-wrapper">
            <h5 className="footer-content-title">Hubungi Kami</h5>
            <p className="footer-content-list">Telp: 0274-2885822</p>
            <p className="footer-content-list">WA: 0895422447394</p>
            <p className="footer-content-list">Senin - Minggu</p>
            <p className="footer-content-list">24 Jam Non Stop</p>
            <p className="footer-content-list visibility-none">Spacer</p>
            <p className="footer-content-list">Jl. Palagan Tentara Pelajar</p>
            <p className="footer-content-list">No 81 Jongkang, Sariharjo,</p>
            <p className="footer-content-list">Ngaglik, Sleman</p>
            <p className="footer-content-list">Daerah Istimewa Yogyakarta</p>
            <p className="footer-content-list">55581</p>
          </div>
          <div className="footer-content-list-wrapper">
            <h5 className="footer-content-title">Layanan</h5>
            <p className="footer-content-list">Domain</p>
            <p className="footer-content-list">Shared Hosting</p>
            <p className="footer-content-list">Cloud Hosting</p>
            <p className="footer-content-list">Cloud VPS hosting</p>
            <p className="footer-content-list">Transfer Hosting</p>
            <p className="footer-content-list">Web Builder</p>
            <p className="footer-content-list">Keamanan SSL/HTTPS</p>
            <p className="footer-content-list">Jasa Pembuatan Website</p>
            <p className="footer-content-list">Program Afiliasi</p>
            <p className="footer-content-list">Who is</p>
            <p className="footer-content-list">Niagahoster Status</p>
          </div>
          <div className="footer-content-list-wrapper">
            <h5 className="footer-content-title">Service Hosting</h5>
            <p className="footer-content-list">Hosting Murah</p>
            <p className="footer-content-list">Hosting Indonesia</p>
            <p className="footer-content-list">Hosting Singapore SG</p>
            <p className="footer-content-list">Hosting Wordpress</p>
            <p className="footer-content-list">Email Hosting</p>
            <p className="footer-content-list">Reseller Hosting</p>
            <p className="footer-content-list">Web Hosting Unlimited</p>
          </div>
          <div className="footer-content-list-wrapper">
            <h5 className="footer-content-title">Kenapa Pilih Niagahoster?</h5>
            <p className="footer-content-list">Hosting Terbaik</p>
            <p className="footer-content-list">Data center Hosting Terbaik</p>
            <p className="footer-content-list">Domain Gratis</p>
            <p className="footer-content-list">Bagi-bagi Domain Gratis</p>
            <p className="footer-content-list">Bagi-bagi Hosting Gratis</p>
            <p className="footer-content-list">Review Pelanggan</p>
          </div>
        </div>
        <div className="footer-content-wrapper flex">
          <div className="footer-content-list-wrapper">
            <h5 className="footer-content-title">Tutorial</h5>
            <p className="footer-content-list">Ebook Gratis</p>
            <p className="footer-content-list">Knowledgebase</p>
            <p className="footer-content-list">Blog</p>
            <p className="footer-content-list">Cara Pembayaran</p>
            <p className="footer-content-list">Niaga Course</p>
          </div>
          <div className="footer-content-list-wrapper">
            <h5 className="footer-content-title">Tentang Kami</h5>
            <p className="footer-content-list">Tentang</p>
            <p className="footer-content-list">Penawaran & Promo Spesial</p>
            <p className="footer-content-list">Niaga Poin</p>
            <p className="footer-content-list">Karir</p>
            <p className="footer-content-list">kontak Kami</p>
          </div>
          <form action="" className="footer-content-list-wrapper">
            <h5 className="footer-content-title">
              Newsletter
            </h5>
            <input
              type="email"
              placeholder="youremail@mail.com"
              name="email"
              id="email"
            />
            <hr />
            <div className="newsletter-btn-wrapper">
              <Link to="/" className="btn-newsletter orange">berlangganan</Link>
            </div>
          </form>
          <div class="footer-content-list-wrapper">
            <div class="footer-icon-wrapper">
              <Link to="/"><i class="fab fa-facebook"></i></Link>
              <Link to="/"><i class="fab fa-instagram"></i></Link>
              <Link to="/"><i class="fab fa-linkedin"></i></Link>
              <Link to="/"><i class="fab fa-twitter"></i></Link>
            </div>
          </div>
        </div>
        <p className="copyright">
          Copyright 2019 Niagahoster | Hosting powered by PHP7, CloudLinux,
          CloudFlare, BitNinja and DC DCI-Indonesia. Cloud VPS Murah powered by
          Webuzo Softaculous, Intel SSD and cloud computing technology.
        </p>
        <Link to="/" className="terms-condition"
          >Syarat dan Ketentuan | Kebijakan Privasi</Link
        >
      </div>
    </footer>
  
        )
}

export default FooterTop;
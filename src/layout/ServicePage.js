import React from 'react';
import { Link } from 'react-router-dom';
import '../asset/style/ServicePage.css'

function ServicePage(props) {
	const servicepage = props.servicepage.map((item) => (
		<div className='servicepage' key={item.id2}>
		 		<section className='layanan-wrapper'>
		 			<div className='container'>
						<h3 className='heading'>{item.title}</h3>

							 <div className="card-wrapper">
            					<div className="card-column-wrapper flex">

              						<div className="card layanan">
                						<figure className="layanan-icon-wrapper">
                						<img src={item.imageicon1} alt='' className='layanan-icon'/>
                						</figure>
                						<figcaption>{item.titleCard}</figcaption>
										<p className='layanan-deskripsi'>{item.description}</p>
										<p className='layanan-price-intro>'>{item.descriptionChild}</p>
										<p className='layanan-price'>{item.price}</p>
									</div>

									<div className="card layanan">
                						<figure className="layanan-icon-wrapper">
                						<img src={item.imageicon2} alt='' className='layanan-icon'/>
                						</figure>
                						<figcaption>{item.titleCard2}</figcaption>
										<p className='layanan-deskripsi'>{item.description2}</p>
										<p className='layanan-price-intro>'>{item.descriptionChild2}</p>
										<p className='layanan-price'>{item.price2}</p>
									</div>

									<div className="card layanan">
                						<figure className="layanan-icon-wrapper">
                						<img src={item.imageicon3} alt='' className='layanan-icon'/>
                						</figure>
                						<figcaption>{item.titleCard3}</figcaption>
										<p className='layanan-deskripsi'>{item.description3}</p>
										<p className='layanan-price-intro>'>{item.descriptionChild3}</p>
										<p className='layanan-price'>{item.price3}</p>
									</div>
							 		
							 		<div className="card layanan">
                						<figure className="layanan-icon-wrapper">
                						<img src={item.imageicon4} alt='' className='layanan-icon'/>
                						</figure>
                						<figcaption>{item.titleCard4}</figcaption>
										<p className='layanan-deskripsi'>{item.description4}</p>
										<p className='layanan-price-intro>'>{item.descriptionChild4}</p>
										<p className='layanan-price'>{item.price4}</p>
									</div>
								</div>

									<div className='card layanan-row'>
									
                						<figure className='layanan-icon-wrapper'>
                						<img src={item.imageicon5} alt='' className='layanan-icon'/>
                						</figure>
                						<div className='description-wrapper-row-card'>
                						<figcaption className='layanan-row'>{item.titleCard5}</figcaption>
										<p className='layanan-deskripsi'>{item.description5}
											<Link to='/' className='a'>{item.descriptionChild5}</Link>
										</p>
									</div>
            					</div>
          					</div>
       					</div>
      				</section>
      	</div>

		));

		return (
			<div>
				{servicepage}
			</div>	
			)
		}

export default ServicePage;
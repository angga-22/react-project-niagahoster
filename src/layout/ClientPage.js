import React from 'react';
import '../asset/style/ClientPage.css'

function ClientPage(props) {
	const clientpage = props.clientpage.map((item) => (
		<div className='clientpage' key={item.id7}>
			<section className='client-wrapper'>
				<h3 className='heading'>
					{item.title}
				</h3>

				<div className='container'>
					<div className='client-list-wrapper flex'>
						<img src={item.image1} alt='' className='client-img'/>
						<img src={item.image2} alt='' className='client-img'/>
						<img src={item.image3} alt='' className='client-img'/>
						<img src={item.image4} alt='' className='client-img'/>
						<img src={item.image5} alt='' className='client-img'/>
					</div>
				</div>
			</section>
			
		</div>
		));

		return (
			<div>
				{clientpage}
			</div>	
			)
		}

export default ClientPage;

import React from 'react';
import { Link } from 'react-router-dom';
import '../asset/style/SecurityPage.css'

function SecurityPage(props) {
	const securitypage = props.securitypage.map((item) => (
		<div className='securitypage' key={item.id3}>
			<section className='priority-wrapper'>
				<div className='container'>
					<h3 className='heading'>
						{item.title}
					</h3>

					<div className='priority-content-wrapper flex'>
						<div className='priority-feature-wrapper'>
						
							<div className='priority-feature'>
								<img src={item.imageaa1} alt='' className='image'/>
								<h4 className='priority-feature-title'>
									{item.titleCard}
								</h4>
								<p className='priority-description'>
									{item.description}
								</p>
							</div>

							<div className='priority-feature'>
								<img src={item.imageaa2} alt='' className='image'/>
								<h4 className='priority-feature-title'>
									{item.titleCard1}
								</h4>
								<p className='priority-description'>
									{item.description1}
								</p>
							</div>

							<Link to='/' className='btn-hero orange'>{item.seeMore}</Link>

							<div className='priority-image-wrapper'>
								<img src={item.imagea1} alt='' className='priority-server-img'/>
								<img src={item.imagea2} alt='' className='priority-graphic-img'/>
								<img src={item.imagea3} alt='' className='priority-imunify-img'/>
								<img src={item.imagea4} alt='' className='priority-speed-img'/>
							</div>
						</div>
					</div>
				</div>
			</section>							
		</div>
		));

		return (
			<div>
				{securitypage}
			</div>	
			)
		}

export default SecurityPage;
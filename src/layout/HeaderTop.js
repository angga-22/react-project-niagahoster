import React from 'react';
import { Link } from 'react-router-dom';
import image from '../asset/image/nh-logo.svg';
import '../asset/style/HeaderTop.css';
import '../asset/style/All.css';




const HeaderTop = () => {
    return (
         <div className="nav-wrapper">
              <div className="container">
                <nav className="sub-nav">
                  <span><i className="fas fa-phone"></i>0274-2885822</span>
                  <span><i className="fas fa-comment-alt"></i>Live Chat</span>
                  <i className="fas fa-shopping-cart"></i>
                </nav>
                <nav className="main-nav flex">
                  <Link to="" className="nav-brand-wrapper">
                    <img src={image} alt=''/>
                  </Link>
                  <div className="navigation flex">
                    <Link to ='/unlimited'>UNLIMITED HOSTING</Link>
                    <Link to ='/hosting'>CLOUD HOSTING</Link>
                    <Link to ='/VPS'>CLOID VPS</Link>
                    <Link to ='/domain'>DOMAIN</Link>
                    <Link to ='/afiliasi'>AFILIASI</Link>
                    <Link to ='/blog'>BLOG</Link>
                    <button className='login'> LOGIN </button>
                  </div>
                </nav>
              </div>
         </div>

        )
    }

    export default HeaderTop;

 
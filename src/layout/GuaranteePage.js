import React from 'react';
import { Link } from 'react-router-dom';
import '../asset/style/GuaranteePage.css'

function GuaranteePage(props) {
	const guaranteepage = props.guaranteepage.map((item) => (
		<div className='guaranteepage' key={item.id9}>
			<section className='guarantee-wrapper'>
				<div className='container'>

					<div className='guarantee-content-wrapper' >
						<img src={item.image} alt='' className='guarantee-img'/>
					</div>

					<div className='guarantee-description-wrapper'>
						<h3 className='heading'>{item.title}</h3>
						<p className='guarantee-description'>{item.description}</p>
						<Link to='/' class='orange'>{item.button}</Link>
					</div>

				</div>
			</section>
			
		</div>
		));

		return (
			<div>
				{guaranteepage}
			</div>	
			)
		}

export default GuaranteePage;

	
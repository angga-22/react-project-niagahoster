import React from 'react';
import '../asset/style/PaymentPage.css'

function PaymentPage(props) {
	const paymentpage = props.paymentpage.map((item) => (
		<div className='paymentpage' key={item.id10}>
			<section className='payment-wrapper'>
				<h2 className='payment-title'>{item.title}</h2>
				<div className='payment-img-wrapper flex'>
					<img src={item.image1} alt='' className='payment-img'/>
					<img src={item.image2} alt='' className='payment-img'/>
					<img src={item.image3} alt='' className='payment-img'/>
					<img src={item.image4} alt='' className='payment-img'/>
					<img src={item.image5} alt='' className='payment-img'/>
					<img src={item.image6} alt='' className='payment-img'/>
					<img src={item.image7} alt='' className='payment-img'/>
					<img src={item.image8} alt='' className='payment-img'/>
					<img src={item.image9} alt='' className='payment-img'/>
					<img src={item.image10} alt='' className='payment-img'/>
					<img src={item.image11} alt='' className='payment-img'/>
					<img src={item.image12} alt='' className='payment-img'/>
					<img src={item.image13} alt='' className='payment-img'/>
					<img src={item.image14} alt='' className='payment-img'/>
					<img src={item.image15} alt='' className='payment-img'/>
					<img src={item.image16} alt='' className='payment-img'/>
					<img src={item.image17} alt='' className='payment-img'/>
				</div>
			</section>

			
		</div>
		));

		return (
			<div>
				{paymentpage}
			</div>	
			)
		}

export default PaymentPage;


		
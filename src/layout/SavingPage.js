import React from 'react';
import '../asset/style/SavingPage.css'


function SavingPage(props) {
	const savingpage = props.savingpage.map((item) => (
		<div className='savingpage' key={item.id4}>
			<section className="cost-saving-wrapper">
        		<div className="container">
          			<h3 className="heading">{item.title}</h3>
	          		<div className="cost-saving-content-wrapper flex">
	            		<div className="cost-saving-image-wrapper">
	            			<img src={item.imageb} alt='' className='cost-titis-img'/>
	            			<img src={item.imageb1} alt='' className='cost-woman1-img'/>
	            			<img src={item.imageb2} alt='' className='cost-woman2-img'/>
	            			<img src={item.imageb3} alt='' className='cost-man-img'/>
	            			<img src={item.imageb4} alt='' className='cost-online-img'/>
	            			<img src={item.imageb5} alt='' className='cost-intercom-img'/>
	            		</div>

	            		<div className="cost-saving-feature-wrapper">

	              			<div className="cost-saving-feature">
	              				<img src={item.imagebb} alt='' className='bb'/>
	              				<h4 className='cost-saving-feature-title'>{item.ptitle}</h4>
	              				<p className='cost-saving-description'>{item.pdesc}
	              				</p>
	              			</div>

	              			<div className="cost-saving-feature">
	              				<img src={item.imagebb1} alt='' className='bb'/>
	              				<h4 className='cost-saving-feature-title'>{item.ptitle1}</h4>
	              				<p className='cost-saving-description'>{item.pdesc1}
	              				</p>
	              			</div>

	              			<div className="cost-saving-feature">
	              				<img src={item.imagebb2} alt='' className='bb'/>
	              				<h4 className='cost-saving-feature-title'>{item.ptitle2}</h4>
	              				<p className='cost-saving-description'>{item.pdesc2}
	              				</p>
	              			</div>

              			</div>
              		</div>
            	</div>
        	</section>
		</div>
		));

		return (
			<div>
				{savingpage}
			</div>	
			)
		}

export default SavingPage;
import React from  'react';
import { Link } from 'react-router-dom';
import '../asset/style/HostingPage.css';



function HostingPage(props) {
	const hostingpack = props.hostingpack.map((item) => (
		<div className='hostingpack' key={item.id6}>
			<section className='package-wrapper'>
				<h3 className='heading'>
					Pilih Paket Hosting Anda
				</h3>

				<div className='container'>
					<div className='card-wrapper flex'>

						<div className='card-package'>
								<p className='package-label'>{item.discounttitle}</p>
								<h5 className='package-name'>{item.namediscount}</h5>
								<p className='package-discount visibility-none'>x</p>
								<p className='package-price'>{item.price}</p>
							
								<Link to='/' className='orange'>{item.button}</Link>
								<package className='package-description'>{item.description}</package>
							<div className='package-feature-wrapper'>
								<p className='package-feature'> 500 MB Disk Space </p>
								<p className='package-feature'> Unlimited Bandwidth </p>
								<p className='package-feature'> Unlimited Database </p>
								<p className='package-feature'> 1 Domain </p>
								<p className='package-feature'> Instant Backup </p>
								<p className='package-feature'> Unlimited SSL gratis selamanya </p>
							</div>
							<Link to='/' className='package-feature-detail'>{item.detail}</Link>
						</div>

						<div className='card-package'>
								<p className='package-label'>{item.discounttitle1}</p>
								<h5 className='package-name'>{item.namediscount1}</h5>
								<p className='package-discount visibility-none'>x</p>
								<p className='package-price'>{item.price1}</p>
							
								<Link to='/' className='orange'>{item.button}</Link>
								<package className='package-description'>{item.description1}</package>
							<div className='package-feature-wrapper'>
								<p className="package-feature">Unlimited Disk Space</p>
								<p className="package-feature">Unlimited Bandwidth</p>
								<p className="package-feature">Unlimited POP3 Email</p>
								<p className="package-feature">Unlimited Database</p>
								<p className="package-feature">10 Addon Domain</p>
								<p className="package-feature">Instant Backup</p>
								<p className="package-feature">Domain Gratis</p>
								<p className="package-feature">Unlimited SSL Gratis Selamanya</p>
							<Link to='/' className='package-feature-detail'>{item.detail}</Link>
							</div>
						</div>

						<div className='card-package'>
								<p className='package-label'>{item.discounttitle2}</p>
								<h5 className='package-name'>{item.namediscount2}</h5>
								<p className='package-discount visibility-none'>x</p>
								<p className='package-price'>{item.price2}</p>
								
								<Link to='/' className='orange'>{item.button}</Link>
								<package className='package-description'>{item.description2}</package>
							<div className='package-feature-wrapper'>
								<p className="package-feature">Unlimited Disk Space</p>
								<p className="package-feature">Unlimited Bandwidth</p>
								<p className="package-feature">Unlimited POP3 Email</p>
								<p className="package-feature">Unlimited Database</p>
								<p className="package-feature">Unlimited Addon Domain</p>
								<p className="package-feature">Instant Backup</p>
								<p className="package-feature">Domain Gratis</p>
								<p className="package-feature">Unlimited SSL Gratis Selamanya</p>
								<p className="package-feature">SpamAsassin Mail Protection</p>
							<Link to='/' className='package-feature-detail'>{item.detail}</Link>
							</div>
						</div>
						
						<div className='card-package'>
								<p className='package-label'>{item.discounttitle3}</p>
								<h5 className='package-name'>{item.namediscount3}</h5>
								<p className='package-discount visibility-none'>x</p>
								<p className='package-price'>{item.price3}</p>
							
								<Link to='/' className='orange'>{item.button}</Link>
								<package className='package-description'>{item.description3}</package>
							<div className='package-feature-wrapper'>
								<p className="package-feature">Unlimited Disk Space</p>
								<p className="package-feature">Unlimited Bandwidth</p>
								<p className="package-feature">Unlimited POP3 Email</p>
								<p className="package-feature">Unlimited Database</p>
								<p className="package-feature">Unlimited Addon Domain</p>
								<p className="package-feature">Magic Auto Backup & Restore</p>
								<p className="package-feature">Domain Gratis</p>
								<p className="package-feature">Unlimited SSL Gratis Selamanya</p>
								<p className="package-feature">Prioritas Layanan Support</p>
								<p className="package-feature">Spam Asassin Mail Protection</p>
							<Link to='/' className='package-feature-detail'>{item.detail}</Link>
							</div>
						</div>
					</div>
				</div>
			</section>		
		</div>
		));

		return (
			<div>
				{hostingpack}
			</div>	
			)
		}

export default HostingPage;
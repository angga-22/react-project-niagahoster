import React from 'react';
import { Link } from 'react-router-dom';
import '../asset/style/FAQPage.css';
 
function FAQPage(props) {
	const faqpage = props.faqpage.map((item) => (
		<div className='faqpage' key={item.id}>
			<section className='faq-wrapper'>
				<h3 className='heading'>{item.title}</h3>
					<div className='container'>
						<ul className='question-wrapper'>
							<li className='question'>{item.label1}</li>
							
							<hr></hr>
							<li className='question'>{item.label2}</li>	
							<hr></hr>					
							<li className='question'>{item.label3}</li>	
							<hr></hr>					
							<li className='question'>{item.label4}</li>	
							<hr></hr>					
							<li className='question'>{item.label5}</li>	
							<hr></hr>					
							<li className='question'>{item.label6}</li>	
							<hr></hr>					
							<li className='question'>{item.label7}</li>		
							<hr></hr>				
						</ul>
					</div>
			</section>	
		</div>
	));

		return (
			<div>
				{faqpage}
			</div>	
			)
		}

export default FAQPage;


	
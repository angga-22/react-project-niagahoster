import React from 'react';
import { Link } from 'react-router-dom';
import '../asset/style/CustomerPage.css'

function CustomerPage(props) {
	const customerpage = props.customerpage.map((item) => (
		<div className='customerpage' key={item.id5}>
			<section class="customer-wrapper">
        		<div class="container">
        			<h3 className='heading'>{item.title}</h3>

	        		<div className='customer-content-wrapper flex'>

	        			<div className='customer'>
	        				<img src={item.image} alt='' className='devjavu-img'/>
	        				<p className='customer-testimony'>{item.description}</p>
	        				<p className='owner'>{item.nameowner}<span>{item.owner} </span>
	        				</p>
	        			</div>

	        			<div className='customer'>
	        				<img src={item.image1} alt='' className='devjavu-img'/>
	        				<p className='customer-testimony'>{item.description1}</p>
	        				<p className='owner'>{item.nameowner1}<span>{item.owner1} </span>
	        				</p>
	        			</div>

	        			<div className='customer'>
	        				<img src={item.image2} alt='' className='devjavu-img'/>
	        				<p className='customer-testimony'>{item.description2}</p>
	        				<p className='owner'>{item.nameowner2}<span>{item.owner2} </span>
	        				</p>
	        			</div>
	        		</div>
	        	</div>
	        </section>
		</div>
		));

		return (
			<div>
				{customerpage}
			</div>	
			)
		}

export default CustomerPage;
import React from "react";
import { Route } from "react-router-dom";
import HeaderTop from "../layout/HeaderTop"
import FooterBottom from "../layout/FooterBottom"
import Card from '../component/Card'
import UnlimitedHosting from '../view/UnlimitedHosting'
import CloudHosting from '../view/CloudHosting'
import CloudVps from '../view/CloudVps';
import Domain from '../view/Domain'
import Afiliasi from '../view/Afiliasi'
import Blog from '../view/Blog'


const Routes = () => {
    return (
        <div className='App'>
            <HeaderTop/>
            	<Route path="/unlimited" component={UnlimitedHosting} exact />
            	<Route path="/hosting" component={CloudHosting} exact />
            	<Route path="/VPS" component={CloudVps} exact />
            	<Route path="/domain" component={Domain} exact />
            	<Route path="/afiliasi" component={Afiliasi} exact />
            	<Route path="/blog" component={Blog} exact />
            <Card/>
            <FooterBottom/>
        </div>
        )
}

export default Routes;
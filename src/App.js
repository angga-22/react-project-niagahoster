import React, { useState } from 'react';
import './App.css';
import Routes from './router/Routes';

const App = () => {
  return (
    <div>
      <Routes/>
    </div>
    )
}

export default App;